<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Court extends Model
{
    use HasFactory;

    public function tipo()
    {
        return $this->belongsTo(CourtType::class);
    }

    public function sucursal()
    {
        return $this->belongsTo(Branch::class);
    }
}
