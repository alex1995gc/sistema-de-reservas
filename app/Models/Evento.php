<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    use HasFactory;

    public function cancha()
    {
        return $this->hasOne(Court::class, "id", "court_id");
    }
}
