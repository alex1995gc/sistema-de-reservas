@extends('layouts.plantillabase')

@section('contenido')
@if(Auth::user()->id == 1)
<h2>EDITAR REGISTROS</h2>

<form action="/court_types/{{$court_type->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="mb-3">
    <label for="" class="form-label">Tipo de Cancha</label>
    <input id="tipo_cancha" name="tipo_cancha" type="text" class="form-control" tabindex="1"  value="{{$court_type->tipo}}" required>
  </div>
  <a href="/court_types" class="btn btn-secondary" tabindex="3">Cancelar</a>
  <button type="submit" class="btn btn-primary" tabindex="2">Guardar</button>
</form>
@endif
@if(Auth::user()->id != 1)
  <h1 style="text-align: center;">No tienes permisos para esta acción</h1>
@endif
@endsection