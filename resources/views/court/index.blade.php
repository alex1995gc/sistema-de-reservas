@extends('layouts.plantillabase')

@section('css')
  <link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css" rel="stylesheet">
@endSection

@section('contenido')
@if(Auth::user()->id == 1)
<a href="courts/create" class="btn btn-primary mb-2">CREAR</a>
@endif

<table id="canchas" class="table table-dark table-striped mt-4" >
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Número de Cancha</th>
      <th scope="col">Descripción</th>
      <th scope="col">Sucursal</th>
      <th scope="col">Tipo de Cancha</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>    
    @foreach ($courts as $court)
    <tr>
        <td>{{$court->id}}</td>
        <td>{{$court->numero_de_cancha}}</td>
        <td>{{$court->descripcion}}</td>
        <td>{{$court->sucursal_id}}</td>
        <td>{{$court->tipo_id}}</td>
        <td>
         <form action="{{ route('courts.destroy',$court->id) }}" method="POST">
         @if(Auth::user()->id == 1)
          <a href="/courts/{{$court->id}}/edit" class="btn btn-info">Editar</a>  
         @endif       
              @csrf
              @method('DELETE')
          @if(Auth::user()->id == 1)
          <button type="submit" class="btn btn-danger">Delete</button>
          @endif
         </form>          
        </td>        
    </tr>
    @endforeach
  </tbody>
</table>
@section('js')
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#canchas').DataTable({
      "lengthMenu": [[5, 10, 50, -1], [5, 10, 25, 50, "All"]]
      });
    });
  </script>
@endSection
@endSection