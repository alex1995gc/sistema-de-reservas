<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('court_types','\App\Http\Controllers\CourtTypeController');

Route::resource('branches','\App\Http\Controllers\BranchController');

Route::resource('courts','\App\Http\Controllers\CourtController');

Route::resource('evento','\App\Http\Controllers\EventoController');

Route::resource('users','\App\Http\Controllers\UserController');

Auth::routes();

//Route::get('/','App\Http\Controllers\EventoController@index');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
