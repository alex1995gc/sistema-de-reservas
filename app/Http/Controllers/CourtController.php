<?php

namespace App\Http\Controllers;

use App\Models\Court;
use App\Models\CourtType;
use App\Models\Branch;
use Illuminate\Http\Request;

class CourtController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courts = Court::all();
        return view('court.index')->with('courts', $courts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $court = new Court();
        $court_types = CourtType::pluck('tipo', 'id');
        $branches = Branch::pluck('nombre', 'id');
        return view('court.create', compact('court', 'court_types'), compact('court', 'branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $courts = new Court();
        $courts->numero_de_cancha = $request->get('numero_cancha');
        $courts->descripcion = $request->get('descripcion');
        $courts->sucursal_id = $request->get('sucursal');
        $courts->tipo_id = $request->get('tipo');
        $courts->save();
        return redirect('/courts');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Court  $court
     * @return \Illuminate\Http\Response
     */
    public function show(Court $court)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $court = Court::find($id);
        $court_types = CourtType::pluck('tipo', 'id');
        $branches = Branch::pluck('nombre', 'id'); 
        return view('court.edit', compact('court', 'court_types'), compact('court', 'branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $court = Court::find($id);
        $court->numero_de_cancha = $request->get('numero_cancha');
        $court->descripcion = $request->get('descripcion');
        $court->tipo_id = $request->get('tipo');
        $court->sucursal_id = $request->get('sucursal');
        $court->save();

        return redirect('/courts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $court = Court::find($id);
        $court->delete();
        return redirect('/courts');
    }
}
