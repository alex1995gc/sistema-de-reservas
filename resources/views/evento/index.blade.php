@extends('layouts.app')
@section('content')
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<!-- Button trigger modal -->
<!--<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Datos del evento</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="d-none">
          ID:
          <input type="text" name="txtID" id="txtID">
          <br>
          Fecha:
          <input type="text" name="txtFecha" id="txtFecha">
        </div>

        <div class="form-row">
          <div class="form-group col-md-12">
            <label>Título:</label>
            <input type="text" class="form-control" name="txtTitulo" id="txtTitulo">
          </div>
          <div class="form-group col-md-6">
            <label>Hora Inicial:</label>
            <input type="time" min="07:00" max="23:00" step="600" class="form-control" name="txtHora" id="txtHora">
          </div>
          <div class="form-group col-md-6">
          <label>Hora Final:</label>
            <input type="time" min="07:00" max="23:00" step="600" class="form-control" name="txtHoraF" id="txtHoraF">
          </div>
          <div class="form-group col-md-12">
            <label>Nombre del cliente:</label>
            <textarea name="txtDescripcion" class="form-control" id="txtDescripcion" cols="30" rows="3"></textarea>
          </div>

          <div class="form-group col-md-12">
            <label>Sucursal:</label>
            <select class="form-control" id="selectSucursal" autocomplete="off">
              @foreach ($sucursales as $sucursal)
                <option value="{{$sucursal->id}}">
                  {{ $sucursal->nombre }}
                </option>
              @endforeach
            </select>
          </div>

          <div class="form-group col-md-12">
            <label>Tipo de cancha:</label>
            <select class="form-control" id="selectCourtType" autocomplete="off">
              @foreach ($types as $type)
                <option value="{{$type->id}}">
                  {{ $type->tipo }}
                </option>
              @endforeach
            </select>
          </div>

          <div class="form-group col-md-12">
            <label>Cancha:</label>
            <select name="court_id" class="form-control" id="selectCourt" autocomplete="off">              
            </select>
          </div>

        </div>
        
      </div>
      <div class="modal-footer">
        <button id="btnAgregar" class="btn btn-success">Agregar</button>
        @if(Auth::user()->id == 1)
        <button id="btnModificar" class="btn btn-warning">Modificar</button>
        @endif
        @if(Auth::user()->id == 1)
        <button id="btnBorrar" class="btn btn-danger">Borrar</button>
        @endif
        <button id="btnCancelar" data-bs-dismiss="modal" class="btn btn-primary">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<div class="container">
    
    <div id="agenda">
         
    </div>

</div>
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
  
  function validarDateTime(){
    
    let today = new Date();
    today.setSeconds(0)
    today.setMilliseconds(0)
    let start_text = $('#txtFecha').val() + " " + $('#txtHora').val() + ":00"
    let end_text = $('#txtFecha').val() + " " + $('#txtHoraF').val() + ":00"

    let since = new Date(start_text);
    let until = new Date(end_text);
  

    if(until.getHours() >= 23 && until.getMinutes() > 0){
      alert("No se puede reservar mas alla de las 23:00 hs.")
      return false;
    }

    let min_reservation = new Date(today);
    min_reservation.setHours(min_reservation.getHours() + 1);

    if(since.getTime() <= min_reservation.getTime()){
      alert("La reservacion requiere una hora de anticipacion como minimo.")
      return false;
    }

    return true;
  }

  function poblarCancha(listado){
    $("#selectCourt").empty()
    if(listado.length == 0){
      $("#selectCourt").attr("disabled", true);
    }else {
      $("#selectCourt").removeAttr("disabled");
    }
    clean = true;
    first_value = null;
    current_value = $("#selectCourt").val()
    for(let i = 0; i< listado.length; i++){
      let opcion = listado[i]
      let tag = document.createElement("option")
      if(first_value == null){
        first_value = opcion.id
      }
      tag.value = opcion.id;
      tag.innerHTML = opcion.descripcion
      $("#selectCourt").append(tag)
      clean == clean && opcion.id != current_value
    }
    $("#selectCourt").val(first_value)
  }


  function refreshCanchas(){
    let tipo_id = $("#selectCourtType").val()
    let sucursal_id = $("#selectSucursal").val()
    let tipos = {!! json_encode($types) !!}

    for(let i = 0; i < tipos.length; i++){
      let tipo = tipos[i]
      if(tipo.id == tipo_id){
        let lista = tipo.canchas.filter(function (val){
          return val.sucursal_id == sucursal_id;
        })
        poblarCancha(lista)
        return;
      }
    }
  }


  document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('agenda');
        var today = new Date().toISOString().slice(0,10);
        var calendar = new FullCalendar.Calendar(calendarEl, {
          initialView: 'dayGridMonth',
          aspectRatio: 1.5,
          headerToolbar: {
            left: 'prev,next',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
          },
          validRange: {
            start: today
          },  
         editable: true,
         locale: 'es', 
         selectable: true,
         forceEventDuration: true,
         dateClick: function(info) {
            //alert('visor de eventos ' + info.dateStr);
            limpiarFormulario();
            $('#txtFecha').val(info.dateStr);
            $('#btnAgregar').prop('disabled',false)
            $('#btnModificar').prop('disabled',true)
            $('#btnBorrar').prop('disabled',true)
            $('#exampleModal').modal('toggle');
            /*calendar.addEvent({
              title: 'evento x',
              date: info.dateStr
            })*/
          },
          eventClick: function(info){
            $('#btnAgregar').prop('disabled',true)
            $('#btnModificar').prop('disabled',false)
            $('#btnBorrar').prop('disabled',false)
            console.log(info)
            console.log(info.event.title)
            $('#txtID').val(info.event.id)
            $('#txtTitulo').val(info.event.title)
            $('#txtDescripcion').val(info.event.extendedProps.descripcion)

            mes = (info.event.start.getMonth()+1)
            dia = (info.event.start.getDate())
            anho = (info.event.start.getFullYear())

            mes = (mes<10)?"0"+mes:mes;
            dia = (dia<10)?"0"+dia:dia;
            hora = info.event.start.getHours()
            minutos = info.event.start.getMinutes() 

            hora = (hora<10)?"0"+hora:hora;
            minutos = (minutos<10)?"0"+minutos:minutos;
            horario = (hora + ":" + minutos);

            $('#txtFecha').val(anho + "-" + mes + "-" + dia)
            $('#txtHora').val(horario)

            let cancha = info.event.extendedProps.cancha
            if(!cancha){
              cancha = {
                id: null,
                tipo_id: {{ $types->first()->id ?? 0 }},
                sucursal_id: {{ $sucursales->first()->id ?? 0 }},
              }
            }
            $("#selectCourt").val(cancha.id)
            $("#selectSucursal").val(cancha.sucursal_id)
            $("#selectCourtType").val(cancha.tipo_id)
            refreshCanchas()
            

            mes2 = (mes<10)?"0"+mes:mes;
            dia2 = (dia<10)?"0"+dia:dia;
            hora2 = info.event.end.getHours()
            minutos2 = info.event.end.getMinutes() 

            hora2 = (hora2<10)?"0"+hora2:hora2;
            minutos2 = (minutos2<10)?"0"+minutos2:minutos2;
            horario2 = (hora2 + ":" + minutos2);

            
            $('#txtHoraF').val(horario2)

            $('#exampleModal').modal('toggle');
          },
          
          /*select: function(info) {
            //alert('Rango Seleccionado ' + info.startStr + ' a ' + info.endStr);
          },*/
          events: "{{ url('/evento/show')}}"       
        });
        calendar.render();

        $('#btnAgregar').click(function(){
          if(!validarDateTime()) return;
          ObjEvento = recolectarDatos("POST");
          EnviarInformacion('',ObjEvento);
        });

        $('#btnBorrar').click(function(){
          ObjEvento = recolectarDatos("DELETE");
          EnviarInformacion('/'+ $('#txtID').val(),ObjEvento);
        });

        $('#btnModificar').click(function(){
          if(!validarDateTime()) return;
          ObjEvento = recolectarDatos("PATCH");
          EnviarInformacion('/'+ $('#txtID').val(),ObjEvento);
        });
        
        function recolectarDatos(method){
          nuevoEvento= {
            title: $('#txtTitulo').val(),
            descripcion:$('#txtDescripcion').val(),
            start:$('#txtFecha').val() + " " + $('#txtHora').val(),
            end:$('#txtFecha').val() + " " + $('#txtHoraF').val(),
            court_id: $("#selectCourt").val(),
            '_token':$("meta[name='csrf-token']").attr("content"),
            '_method':method
          }
          return(nuevoEvento)
        }

        function EnviarInformacion(accion,objEvento){
          $.ajax(
            {
              type:"POST",
              url: "{{ url('/evento') }}" + accion,
              data: objEvento,
              success: function(msg){
                console.log(msg)
                $('#exampleModal').modal('toggle');
                calendar.refetchEvents();
              },
              error: function(){alert("Hay un error, asegurese de haber completado los datos")}
            }
          )
        }

        function limpiarFormulario(){
          $('#txtID').val("")
          $('#txtTitulo').val("")
          $('#txtDescripcion').val("")
          $('#txtFecha').val("")
          $('#txtHora').val("")
          $('#txtHoraF').val("")
        }

        $("#selectSucursal").on('change', refreshCanchas);
        $("#selectCourtType").on('change', refreshCanchas);
        $("#selectSucursal").val({{ $sucursales->first()->id ?? 0 }})
        $("#selectCourtType").val({{ $types->first()->id ?? 0 }})
        refreshCanchas();

      });

</script>
@endsection
