@extends('layouts.plantillabase')

@section('contenido')
@if(Auth::user()->id == 1)
<h2>EDITAR REGISTROS</h2>

<form action="/branches/{{$branch->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="mb-3">
    <label for="" class="form-label">Nombre de la Sucursal</label>
    <input id="nombre" name="nombre" type="text" class="form-control" tabindex="1"  value="{{$branch->nombre}}" required>
  </div>
  <div class="mb-3">
    <label for="" class="form-label">Dirección</label>
    <input id="direccion" name="direccion" type="text" class="form-control" tabindex="2"  value="{{$branch->direccion}}" required>
  </div>
  <div class="mb-3">
    <label for="" class="form-label">Teléfono</label>
    <input id="telefono" name="telefono" type="text" class="form-control" tabindex="3"  value="{{$branch->telefono}}" required>
  </div>
  <a href="/branches" class="btn btn-secondary" tabindex="5">Cancelar</a>
  <button type="submit" class="btn btn-primary" tabindex="4">Guardar</button>
</form>
@endif
@if(Auth::user()->id != 1)
<h1 style="text-align: center;">No tienes permisos para esta acción</h1>
@endif
@endsection