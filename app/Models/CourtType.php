<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourtType extends Model
{
    use HasFactory;

    public function canchas()
    {
        return $this->hasMany(Court::class, "tipo_id");
    }
}
