<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sucursal_id')
                   ->constrained('branches')
                   ->onUpdate('cascade')
                   ->onDelete('cascade');
            $table->foreignId('tipo_id')
                  ->constrained('court_types')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->integer('numero_de_cancha');
            $table->string('descripcion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courts');
    }
}
