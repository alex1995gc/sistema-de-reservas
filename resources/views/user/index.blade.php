@extends('layouts.plantillabase')

@section('css')
<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css" rel="stylesheet">
@endSection

@section('contenido')
@if(Auth::user()->id == 1)
<a href="users/create" class="btn btn-primary mb-2">CREAR</a>
@endif

@if(Auth::user()->id == 1)
<table id="users" class="table table-dark table-striped mt-4">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Email</th>
      <th class=""scope="col">Contraseña</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>    
    @foreach ($users as $user)
    <tr>
        <td>{{$user->id}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td class="">{{$user->password}}</td>
        <td>
         <form action="{{ route('users.destroy',$user->id) }}" method="POST">
           @if(Auth::user()->id == 1)
          <a href="/users/{{$user->id}}/edit" class="btn btn-info">Editar</a>
           @endif        
              @csrf
              @method('DELETE')
          @if(Auth::user()->id == 1)
          <button type="submit" class="btn btn-danger">Delete</button>
          @endif
         </form>          
        </td>        
    </tr>
    @endforeach
  </tbody>
</table>
@endif

@if(Auth::user()->id != 1)
  <h1 style="text-align: center;">No tienes permisos para esta acción</h1>
@endif

@section('js')
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
  <script>
    $(document).ready(function() {
    $('#users').DataTable();
} );
  </script>
@endSection
@endSection