@extends('layouts.plantillabase')

@section('css')
  <link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css" rel="stylesheet">
@endSection

@section('contenido')
@if(Auth::user()->id == 1)
<a href="branches/create" class="btn btn-primary mb-2">CREAR</a>
@endif

<table id="sucursales" class="table table-dark table-striped mt-4">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre de la Sucursal</th>
      <th scope="col">Dirección</th>
      <th scope="col">Teléfono</th>
      <th scope="col">Acciones</th>
      <!--<th scope="col">Usuario</th>!-->
    </tr>
  </thead>
  <tbody>    
    @foreach ($branches as $branch)
    <tr>
        <td>{{$branch->id}}</td>
        <td>{{$branch->nombre}}</td>
        <td>{{$branch->direccion}}</td>
        <td>{{$branch->telefono}}</td>
        <td>
         <form action="{{ route('branches.destroy',$branch->id) }}" method="POST">
          @if(Auth::user()->id == 1)
          <a href="/branches/{{$branch->id}}/edit" class="btn btn-info">Editar</a>
          @endif         
              @csrf
              @method('DELETE')
          @if(Auth::user()->id == 1)
          <button type="submit" class="btn btn-danger">Delete</button>
          @endif
         </form>          
        </td>        
    </tr>
    @endforeach
  </tbody>
</table>
@section('js')
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
  <script>
    $(document).ready(function() {
    $('#sucursales').DataTable();
} );
  </script>
@endSection
@endSection