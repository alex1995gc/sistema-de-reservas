@extends('layouts.plantillabase')

@section('contenido')
@if(Auth::user()->id == 1)
<h2>EDITAR REGISTROS</h2>

<form action="/courts/{{$court->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="mb-3">
    <label for="" class="form-label">Número de Cancha</label>
    <input id="numero_cancha" name="numero_cancha" type="number" class="form-control" tabindex="1" value="{{$court->numero_de_cancha}}" required>
  </div>
  <div class="mb-3">
    <label for="" class="form-label">Descripción</label>
    <input id="descripcion" name="descripcion" type="text" class="form-control" tabindex="2"  value="{{$court->descripcion}}" required>
  </div>
  <div class="form-group">
    {{ Form::label('Tipo de Cancha') }}
    {{ Form::select('court_type_id', $court_types, $court->tipo_id, ['class' =>'form-control' .
       ($errors->has('court_types_id') ? ' is-invalid': ''),
       'id'=>'tipo', 'name'=>'tipo' , 'placeholder'=> 'Tipo de Cancha', 
       'required'])}}
    {!! $errors->first('nombre', '<div class="invalid-feedback">:message</div>') !!}
  </div>
  <div class="form-group">
    {{ Form::label('Sucursal') }}
    {{ Form::select('branch_id', $branches, $court->sucursal_id, ['class' =>'form-control' . 
      ($errors->has('branches_id') ? ' is-invalid': ''),
       'placeholder'=> 'Sucursales', 'id'=>'sucursal', 'name'=>'sucursal',  'required'])}}
    {!! $errors->first('nombre', '<div class="invalid-feedback">:message</div>') !!}
  </div>
  <a href="/courts" class="btn btn-secondary" tabindex="4">Cancelar</a>
  <button type="submit" class="btn btn-primary" tabindex="3">Guardar</button>
</form>
@endif

@if(Auth::user()->id != 1)
<h1 style="text-align: center;">No tienes permisos para esta acción</h1>
@endif
@endsection