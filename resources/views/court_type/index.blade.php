@extends('layouts.plantillabase')

@section('css')
<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css" rel="stylesheet">
@endSection

@section('contenido')
@if(Auth::user()->id == 1)
<a href="court_types/create" class="btn btn-primary mb-2">CREAR</a>
@endif


<table id="tipos" class="table table-dark table-striped mt-4">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Tipo de Cancha</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>    
    @foreach ($court_types as $court_type)
    <tr>
        <td>{{$court_type->id}}</td>
        <td>{{$court_type->tipo}}</td>
        <td>
         <form action="{{ route('court_types.destroy',$court_type->id) }}" method="POST">
           @if(Auth::user()->id == 1)
          <a href="/court_types/{{$court_type->id}}/edit" class="btn btn-info">Editar</a>
           @endif        
              @csrf
              @method('DELETE')
          @if(Auth::user()->id == 1)
          <button type="submit" class="btn btn-danger">Delete</button>
          @endif
         </form>          
        </td>        
    </tr>
    @endforeach
  </tbody>
</table>
@section('js')
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
  <script>
    $(document).ready(function() {
    $('#tipos').DataTable();
} );
  </script>
@endSection
@endSection